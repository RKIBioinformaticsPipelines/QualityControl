  # !/usr/bin/env python  # -*- coding: utf-8 -*-
# created by Stephan Fuchs (FG13, RKI), 2015

'''
Combine FastQC Results
Input: Project/Sample Folder which was created by runQCPipeline (folder contains QCResults - <FastQC_Result>)
Output: html file
'''

# config
version = '0.2.9'
logsep = '=====================================\n'

import os,re
import sys
import time
import shutil
import base64
import tempfile
import matplotlib

matplotlib.use("Agg")  #in case no display is set
import matplotlib.pyplot as plt
import argparse
import numpy as np



#advanced config - HTML output
htmlheader = '<!doctype html>' \
			 '<html>' \
			 '<head>' \
			 '<meta charset="utf-8">' \
			 '<title>Combined FastQC Report</title>' \
			 '<script language="javascript">top.DSfirstAccess = 1442945125;top.DSmaxTimeout=3600;top.DStimediff=1442945348 - parseInt(""+(new Date()).getTime()/1000);</script><SCRIPT language="javascript" id="dsshim" src="/dana-cached/js/shimdata.cgi"></SCRIPT><SCRIPT language="javascript" id="dsfunc" src="/dana-cached/js/oth.js?a4e535923255f21d1261a51909daf29f"></SCRIPT><SCRIPT language="javascript" id="dstimeout" src="/dana-cached/js/sessiontimeout.js?a4e535923255f21d1261a51909daf29f"></SCRIPT><SCRIPT language="javascript" id="dsvar" >' \
			 '//<![CDATA[' \
			 'var dsnodocwrites = 0 ; var DanaCookie="MstrPgLd1=1; MstrPgLd2=1; UserContext=mBoHOGHwbUOz5yuRe-kSp4w1h6L4yNIICPjPhC-9ifhLTYXFL0EmDebAMytIx2kDia-CCTHmi8Y.; tzid=W. Europe Standard Time"; var DSHost="webmail.rki.de"; var DSDanaInfoStr=""; var DSLang="de"; var DSObfuscateHostname=0;var DSTBSettings=17425;var DSTBLU=\'/dana/home/starter.cgi?startpageonly=1\';;danaSetDSHost();' \
			 '//]]>' \
			 '</SCRIPT></head>' \
			 '<body>'

htmlfooter = '</table>\n</body>\n<SCRIPT language="javascript" id="dstb-id">dstb()</SCRIPT></html>\n'

#advanced config - fastqc img extraction
order = ['sld', 'sq', 'sgc', 'bq', 'bnc', 'kmer', 'dup']
imgfiles = {'dup': 'duplication_levels.png',
			'kmer': 'kmer_profiles.png',
			'bgc': 'per_base_gc_content.png',
			'bnc': 'per_base_n_content.png',
			'bq': 'per_base_quality.png',
			'bsc': 'per_base_sequence_content.png',
			'sgc': 'per_sequence_gc_content.png',
			'sq': 'per_sequence_quality.png',
			'sld': 'sequence_length_distribution.png',
			"ptsq": "per_tile_quality.png",
			"ac": "adapter_content.png"
			}
#advanced config - fastqc data extraction
modules = {'sld': '>>Sequence Length Distribution\t', 'sq': '>>Per sequence quality scores\t',
		   'sgc': '>>Per sequence GC content\t'}
all_modules = {'bq': ">>Per base sequence quality",
			   'bsc': ">>Per base sequence content",
			   'bgc': ">>Per base GC content",
			   'bnc': ">>Per base N content",
			   'sld': ">>Sequence Length Distribution",
			   'dup': ">>Sequence Duplication Levels",
			   'kmer': ">>Kmer Content",
			   'sq': '>>Per sequence quality scores',
			   'sgc': '>>Per sequence GC content',
			   'ptsq': '>>Per tile sequence quality',
			   'ac':'>>Adapter Content'
			   }
codes = {'comment': '#', 'modulestart': '>>', 'moduleend': '>>END_MODULE'}

#advanced config - boxplot design

#processing command line arguments
'''parser = argparse.ArgumentParser(prog="COMBINE_FASTQC",
								 description='performs FastQC for multiple fastq files and combines results in a single html file.')
parser.add_argument('file', metavar="FILE(S)", help="input BAM file", type=argparse.FileType('r'), nargs='+')
parser.add_argument('-t', metavar='INT', help="number of threads/CPUs used for FastQC", type=int, action="store",
					default=4)
parser.add_argument('--version', action='version', version='%(prog)s ' + version)

args = parser.parse_args()
'''


#functions
def createCSV(dataset, lines, key, path):
	'''creates CSV used for boxplotting'''
	output = []
	for line in lines:
		line = line.strip()
		fields = line.split('\t')

		#Sequence Length Distribution
		if key == "sld":
			l = fields[0].split('-')
			try:
				mean = (int(l[0]) + int(l[1])) / 2
			except:
				mean = int(l[0])
			output.extend([str(mean)] * int(float(fields[1])))

		#sequence quality or sequence GC content
		if key == "sq" or key == 'sgc':
			v = fields[0]  #quality or gc [%]
			output.extend([str(v)] * int(float(fields[1])))

		#duplicates
		if key == 'dup':
			c = fields[0].replace('>', '')  #count of duplicates
			c = c.replace('k', '000')
			c = c.replace('+', '')
			output.extend([str(c)] * int(float(fields[1]) * 100))
	csvhandle = open(path + '/' + key + ".csv", 'a')
	csvhandle.write(dataset.replace(',', '_') + "," + ",".join(output) + '\n')
	csvhandle.close()


def color(line):
	line = line.strip()
	if line.endswith("pass"):
		return "#3CBC3C"
	elif line.endswith("warn"):
		return "orange"
	elif line.endswith("fail"):
		return "red"
	else:
		return "black"


def get_key(dict, line):
	for key, value in dict.items():
		if line.startswith(value):
			return key
	return None


def barplot(overview, width, height, tmpdir, type, xlabel, ylabel, title):
	bplotfont = {'weight': 'bold', 'size': 5}
	plt.rc('font', **bplotfont)
	fig = plt.figure(1, figsize=(width, height))
	ax = fig.add_subplot(111)
	index = np.arange(overview[1].shape[0])
	opacity = 0.4
	bar_width = 0.35
	bplot = plt.bar(index, overview[1][type], bar_width,
					alpha=opacity,
					color='#385d8a')  #, label='Aligned Reads')
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.title(title)
	plt.xticks(index + (bar_width / 2), [x.decode("utf-8") for x in overview[1]['name']], rotation=90)
	# plt.ylim((0,100))

	for rect, label in zip(bplot, overview[1][type]):
		ax.text(rect.get_x() + rect.get_width() / 2, rect.get_height() , str(label), ha='center', va='bottom')

	fig.savefig(os.path.join(tmpdir ,type + ".png"), bbox_inches='tight', dpi=200)
	fig.clf()


def combineFastQC(project_folder, overview=None, tmp = "tmp"):

	err = 1
	while err > 0:
		timecode = time.strftime("%Y-%m-%d_%H-%M-%S")
		html_fname = "FASTQC_COMBINE_" + timecode + ".html"
		if os.path.isfile(html_fname):
			err += 1
			time.sleep(1)
		else:
			err = 0
		if err == 30:
			print("ERROR: giving up to find available output file name")
			sys.exit(1)
	#working
	try:
		tmpdir = tempfile.mkdtemp(tmp)
		os.makedirs(tmpdir, exist_ok=True)
		chunkfiles = [tmpdir + '/' + html_fname + '1', tmpdir + '/' + html_fname + '2']
		outhandle = open(chunkfiles[1], "w")

		print("Summarize fastqc ", project_folder)
		fastqc_dirs = []
		#sample_name=[]
		for sample in os.listdir(project_folder):
			if os.path.isdir(os.path.join(project_folder, sample)):
				if sample == "QCResults":
					sample = ""
				path = os.path.join(project_folder, sample)
				if "QCResults" in os.listdir(path):
					path = os.path.join(path, "QCResults")

				if "FastQC" in os.listdir(path):
					path = os.path.join(path, "FastQC")

				print("Found path " + path)
				if os.path.exists(path):
					fastqc_folder = sorted( [x for x in os.listdir(path) if x.endswith("_fastqc")])
					if len(fastqc_folder) != 0:
						path_trimmed = os.path.join(path, "..", "Trimmed", "FastQC")
						if os.path.exists(path_trimmed):
							for fastqc in fastqc_folder:
								fastqc_dirs.append(os.path.join(path, fastqc))
								fastqc_dirs.append(os.path.join(path_trimmed, fastqc))
					else:
						fastqc_dirs = fastqc_folder
						fastqc_folder = None

		i = 0
		outhandle.write('<table border = "1"> <tr>')
		fastqc_dirs = sorted(fastqc_dirs, reverse=True)
		for fastqcdir in fastqc_dirs:
			i += 1
			print("extracting data ", fastqcdir)
			store = False
			data = []
			fastqcdatafile = open(os.path.join(fastqcdir, "fastqc_data.txt"), "r")
			frame_color = {}
			for line in iter(fastqcdatafile):
				if line.startswith(codes['comment']):
					pass  #print("comment")
				#continue
				elif line.startswith(codes['moduleend']):
					if len(data) > 0:
						name1 = os.path.basename(fastqcdir)
						if "/Trimmed/" in fastqcdir:
							name1 = "Trimmed " + name1
						createCSV(name1, data[2:], key, tmpdir)
						data = []
						store = False

				elif line.startswith(codes['modulestart']):
					key = get_key(all_modules, line)
					if key in modules.keys():
						store = True
					if key is not None:
						frame_color[key] = color(line)
				if store:
					data.extend([line])
			fastqcdatafile.close()

			#image processing
			name = os.path.basename(fastqcdir)
			if "/Trimmed/" in fastqcdir:
				name = "Trimmed " + os.path.basename(fastqcdir)
			outhandle.write('<tr>\n<td><b>' + name + "</b></td>\n")
			for imgkey in order:
				try:
					with open(os.path.join(fastqcdir , "Images" , imgfiles[imgkey]), "rb") as imgfile:
						imgstring = base64.b64encode(imgfile.read())
						outhandle.write('<td><img style="border: 5px solid ' + frame_color[imgkey] + ';" alt="Embedded Image" src="data:image/png;base64,' + imgstring.decode(
							"utf-8") + '" /></td>\n')
				except:
					outhandle.write('<td></td>\n')
			outhandle.write('</tr>\n')
			if "/Trimmed/" in fastqcdir:
				outhandle.write('</tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>')

		outhandle.write(htmlfooter)
		outhandle.close()

		bplotfont = {'weight': 'bold', 'size': 8}
		plt.rc('font', **bplotfont)

		#generate boxplots
		print ("generating comparative boxplots...")
		xclasses = range(1, len(fastqc_dirs) + 1)
		width = 4.5
		height = 3.5
		if (len(fastqc_dirs) / 3) > width:
			width = len(fastqc_dirs) / 3
			height = len(fastqc_dirs) / 4
			#max width
			if width > 12:
				width = 12
				height = 7
				bplotfont = {'weight': 'bold', 'size': 8}
				plt.rc('font', **bplotfont)

		xlabels = []
		for key in order:
			if key in modules:
				data = []

				plottitle = modules[key].replace(">", '')
				plottitle = plottitle.replace("\t", '')
				print("Plot ", plottitle )

				#csvhandle = open(tmpdir + '/' + key + ".csv", 'r')
				with open(tmpdir + '/' + key + ".csv", 'r') as csvfile:
					csvhandle = csv.reader(csvfile, delimiter=',', quotechar='|')
					for row in csvhandle:
						label = row[0].replace("_fastqc","").replace("concat_","")
						label=re.sub(r"__(\w{8}-\w{8})", "", label)
						label = re.sub(r"_\d{3}","",label)
						xlabels.append(label)
						data.append([float(x) for x in row[1:]])

				fig = plt.figure(1, figsize=(width, height))
				ax = fig.add_subplot(111)
				#bp = ax.boxplot(data, notch=True, patch_artist=True)

				# set props
				'''whisker = {'color':'#b4b4b4', 'linestyle':"-", 'linewidth':0.75}
				caps = {'color':'#b4b4b4', 'linewidth':1}
				flier = {"marker":'o', "lw":0, "color":'#e6e6e6', "markersize":0.5}
				box = {"color":'#385d8a', "linewidth":0.75}'''
				bp = ax.boxplot(data)#,  capsprops = caps , flierprops = flier, boxprops =box)

				for lb, box in zip(label, bp['boxes']):
					#outline color
					if lb.startswith("Trimmed"):
						box.set(color='#385d8a', linewidth=0.75, facecolor='#7092BE')
					else:
						box.set(color='#385d8a', linewidth=0.75)


				for whisker in bp['whiskers']:
					whisker.set(color='#b4b4b4', linestyle="-", linewidth=0.75)

				for cap in bp['caps']:
					cap.set(color='#b4b4b4', linewidth=1)

				for median in bp['medians']:
					median.set(color='#c0504d', linewidth=1)

				for flier in bp['fliers']:
					flier.set(marker='o', lw=0, color='#e6e6e6', markersize=0.5)

				plt.title(plottitle)
				plt.xticks(xclasses, xlabels, rotation=90)
				fig.savefig(tmpdir + '/' + key + ".png", bbox_inches='tight', dpi=200)
				fig.clf()

		if overview is not None:
			try:
				if len([x for x in overview[1]['mapping'] if x != 0.0]) != 0:
					barplot(overview, width, height, tmpdir, 'mapping', 'Samples', 'Mapped reads [%]',
						'Percentage of reads aligned to ' + overview[0])
			except:
				print("Couldnt plot mapping results")
			try:
				barplot(overview, width, height, tmpdir, 'trimming', 'Samples', 'Reads after trimming [%]',
						'Percentage of reads after trimming')
			except:
				print("Couldnt plot trimming results")
			try:
				if len([x for x in overview[1]['shorter_reads'] if x != 0.0]) != 0:
					barplot(overview, width, height, tmpdir, 'shorter_reads', 'Samples', 'Shorter fragments [%]',
							'Percentage of fragments shorter than read length')
			except:
				print("Couldnt plot shorter fragments")
			try:
				if len([x for x in overview[1]['kraken'] if x != 0.0]) != 0:
					barplot(overview, width, height, tmpdir, 'kraken', 'Samples', 'Classified reads [%]',
						'Percentage of classified reads')
			except:
				print("Couldnt plot Kraken results")
		#save boxplots to html
		outhandle = open(chunkfiles[0], "w")
		outhandle.write(htmlheader)
		#for line in iter(outhandle):
		#    if line.strip() == "<boxplotline \>":
		outhandle.write("<tr>\n<td><b>summary</b></td>\n")
		for key in order:
			if key not in modules:
				outhandle.write("<td>-</td>")
			else:
				imgfile = open(tmpdir + '/' + key + ".png", "rb")
				imgstring = base64.b64encode(imgfile.read())
				imgfile.close()
				outhandle.write('<td><img alt="Embedded Image" src="data:image/png;base64,' + imgstring.decode(
					"utf-8") + '" /></td>\n')
		outhandle.write('</tr>\n')

		outhandle.write("<tr>\n<td></td>\n")
		if overview is not None:
			for result in ['mapping', 'trimming', 'shorter_reads', 'kraken']:
				try:
					imgfile = open(os.path.join(tmpdir, result + ".png"), "rb")
					imgstring = base64.b64encode(imgfile.read())
					imgfile.close()
					outhandle.write('<td><img alt="Embedded Image" src="data:image/png;base64,' + imgstring.decode(
						"utf-8") + '" /></td>\n')
				except:
					pass
			outhandle.write('</tr>\n')
			outhandle.close()


		#join sub-htm-files
		print("Create result file:  " + os.path.join(project_folder, html_fname))

		htmlhandle = open(os.path.join(project_folder, html_fname), 'w')
		for chunkfile in chunkfiles:
			chunkhandle = open(chunkfile, 'r')
			for line in iter(chunkhandle):
				htmlhandle.write(line)
			chunkhandle.close()
		htmlhandle.close()
	finally:
		#cleaning
		shutil.rmtree(tmpdir)


import csv

if __name__ == "__main__":
	start  =time.time()
	print(start)
	parser = argparse.ArgumentParser()
	parser.add_argument('-folder', dest='folder', help="Sample folder", required=True)
	parser.add_argument('-csv', dest='csv',
						help="csv file.Format: Sample name | Mapping Result | Trimming Result | Kraken Result | Shorter Fragments ",
						required=False)
	parser.add_argument('-reference', dest="reference", help="Name of reference", required=False)
	arguments = vars(parser.parse_args())

	if arguments["csv"] and arguments["reference"]:

		overview = []
		with open(arguments["csv"], newline='') as csvfile:
			infosheet = csv.reader(csvfile, delimiter=';', quotechar='|')
			for row in infosheet:
				overview.append(tuple(row))
				print(row)
		combineFastQC(arguments["folder"], (arguments["reference"],np.array(overview, dtype = [('name', '|S100'), ('mapping', float), ('trimming', float), ('kraken', float), ('shorter_reads', float)])))

	elif (arguments["csv"] or arguments["reference"]):
		sys.exit("csv and reference required")
	else:
		combineFastQC(arguments["folder"])
	print(time.time() - start)
