#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'LieuV'
__version__ = "1.2.0"

from classes import *
from helper import *
import os
from itertools import groupby
from jinja2 import Environment,FileSystemLoader
import shutil
import configparser
from collections import OrderedDict
import json


#dependencies
try:
	import argparse
except ImportError:
	print('The "argparse" module is not available. Use Python >3.2.')
	sys.exit(1)


parameter = configparser.ConfigParser()
parameter.read(join(dirname(__file__), "parameter.txt"))


file_extensions = parameter["Fileextension"]["fastq"]
r1_pattern = parameter["Pattern"]["R1"]
r2_pattern = parameter["Pattern"]["R2"]
lane_pattern = parameter["Pattern"]["lane"]


def get_illumina_reads(  output, tmp):
	## Add readsets to sample
	readsets = []
	# split by lanes
	if isinstance(arguments["r1"], list):
		for lane in sorted(set([re.search(lane_pattern, x).group() for x in arguments["r1"]])):
			# concat same files
			r1_reads = [x for x in arguments["r1"] if lane in x]
			readname = re.sub(r1_pattern + ".*", "", os.path.basename(r1_reads[0]))
			if len(arguments["r1"]) != 1:
				r1 = FastQFile(join_reads(r1_reads, tmp, readname + "_R1"), [toLatex(os.path.basename(x)) for x in r1_reads]  )
			else:
				r1 = FastQFile(r1_reads[0])
			if arguments["r2"]:
				r2_reads = [x for x in arguments["r2"] if lane in x]

				if len(r2_reads) != 1:
					r2 = FastQFile(join_reads(r2_reads, tmp, readname + "_R2"),[toLatex(os.path.basename(x)) for x in r2_reads] )
				else:
					r2 = FastQFile(r2_reads[0])
				#return [ReadSet( r1, r2)]
				readsets.append(ReadSet(r1,r2))
			else:
				readsets.append(ReadSet(r1))
	else:
		r1 = FastQFile(arguments["r1"])
		if arguments["r2"]:
			r2 = FastQFile(arguments["r2"])
			return [ReadSet(r1,r2)]
		else:
			return [ReadSet(r1)]


	return readsets

def run_analysis (readset, arguments, output, tmp):
	print("Run FastQC...")
	readset.run_FastQC(join(output, "FastQC"))
	if arguments["trimBetter"]:
		trimming_perc = arguments["trimBetter_threshold"]
	else:
		trimming_perc = ""
	readset.run_Trimmomatic(arguments["adapter"], arguments["palindrome"],arguments["minlen"], arguments["trimOption"], trimming_perc)
	readset.trimRes = readset.trimRes.run_FastQC( tmp)
	if arguments["blobplot"]:
		readset.trimRes.run_blobplot(arguments["db"])
	return readset


def check_input(arguments):
	if arguments["r1"]:
		if arguments["r2"]:
			return [arguments["r1"], arguments["r2"]]
		else:
			return [arguments["r1"]]

	if arguments["technology"]=="Illumina":
		if os.path.isdir(arguments["input"]):
			files = os.listdir(arguments["input"])
			files = [os.path.join(arguments["input"], x) for x in files]
			files = [x for x in files if os.path.isfile(x) & any([ext in x for ext in file_extensions])]
			if len(files)==0:
				sys.exit(arguments["input"] + " does not contain fastq files.")
			return files
		else:
			return [arguments["input"]]
	elif arguments["technology"]=="IonTorrent":

		if os.path.isfile(arguments["input"]):

			if arguments["input"].endswith(".bam"):
				return arguments["input"]
			else:
				sys.exit("IonTorrent PGM input has to be a bam-file. Otherwise use -b. Use --help for more information.")
		else:
			sys.exit("Incorrect file input")
	elif arguments["technology"] == "Illumina-MiSeq":
		if os.path.isfile(arguments["input"]):
			if any([arguments["input"].endswith(x) for x in file_extensions]):
				return arguments["input"]
			else:
				sys.exit("Not supported file extension.")
		else:
			sys.exit("Illumina MiSeq input has to be a fastq-file. Otherwise use -b for a batch run. Use --help for more information.")
	else:
		sys.exit("Incorrect file input")
	return None

def getSetname():
	if arguments["technology"] == "Illumina":
		if isinstance(arguments["r1"], list):
			return "_".join(basename(arguments["r1"][0]).split("_")[:-3])
		else:
			return getFilenameWithoutExtension(arguments["r1"], True)
	else:
		return getFilenameWithoutExtension(arguments["r1"], getBase=True)
def fillClasses(temp_bowtie_path = "", tmp=''):
	try:
		output = getDir([arguments["output"], "QCResults"], True)
		sample = Sample(getSetname(), output, arguments["reference"],arguments["threads"], arguments["technology"])
		if arguments["technology"].startswith("Illumina"):
			readsets = get_illumina_reads( output, tmp)
		elif arguments["technology"] == "IonTorrent":
			print("IonTorrent", arguments["r1"])
			fastqfile = bam_to_fastq(arguments["r1"],tmp)
			readsets = [ReadSet( fastqfile)]

		#os.makedirs(tempdir, exist_ok=True)
		for rs in readsets:
			readset = run_analysis(rs, arguments,  output, tmp)
			sample = sample.add_readSet(readset)
		if not arguments["nomapping"]:
			if arguments["save_mapping"]:
				sample.mappingRes = sample.run_Bowtie2(temp_bowtie_path, join(arguments["output"], "QCResults", sample.name +".bam"))
			else:
				sample.mappingRes = sample.run_Bowtie2(temp_bowtie_path, "/dev/null")
		if not arguments["nokraken"]:
			print("Run Kraken.")
			sample = sample.run_Kraken(arguments["db"])

		sample.stop = datetime.datetime.strftime(datetime.datetime.now(),"%d.%m.%Y, %H:%M:%S")
	finally:
		shutil.rmtree(tmp)
	return sample

def writeReport(sample, arguments):
	report_name = os.path.join(sample.mainResultsPath, "Report",
		 "summary_" + sample.name + "_" + datetime.datetime.strftime(datetime.datetime.now(), "%d-%m-%y_%H-%M"))
	print("Writing latex " ,report_name)
	env = Environment()
	env.loader = FileSystemLoader(os.path.dirname(__file__))

	template = env.get_template("report.tex")
	pdf_latex = template.render(sample=sample, pipeline=Pipeline(), trim_param = sample.readSets[0].trimRes.print_param(arguments["palindrome"],arguments["minlen"], arguments["trimOption"]))

	latex = open(report_name + ".tex", "w")
	latex.write(pdf_latex)
	latex.close()

	process = subprocess.Popen(["pdflatex", "-interaction=nonstopmode", "-output-directory=" + join(sample.mainResultsPath, "Report"), report_name + ".tex"], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
	for line in iter(process.stdout.readline, b''):
		pass #print(line)
	for line in iter(process.stderr.readline, b''):
		print(line)

	process.communicate()

	for ext in (".tex",".aux", ".log", ".toc", ".lof", ".lot", ".synctex.gz"):
		try:
			os.remove(report_name + ext)
		except OSError:
			pass

def main(arguments):

	tmp = join(arguments["output"],"QCResults" ,"qc_tmp")
	makedirs(tmp, exist_ok=True)
	makedirs(join(arguments["output"], "QCResults"), exist_ok=True)
	makedirs(join(arguments["output"], "QCResults", "FastQC"), exist_ok=True)
	makedirs(join(arguments["output"], "QCResults", "Trimmed"), exist_ok=True)
	makedirs(join(arguments["output"], "QCResults", "Trimmed", "FastQC"), exist_ok=True)
	makedirs(join(arguments["output"], "QCResults", "Report"), exist_ok=True)
	makedirs(join(arguments["output"], "QCResults", "Report", "src"), exist_ok=True)
	makedirs(join(arguments["output"], "QCResults", "Report", "src", "img"), exist_ok=True)

	# create new boxplot csv files
	for csv in ["Per_sequence_quality_scores.csv", "Per_sequence_GC_content.csv", "Sequence_Length_Distribution.csv"]:
		new_csv = open(join(arguments["output"], "QCResults", "Report", "src", csv), "w")
		new_csv.write("Sample,Read,Lane,Trimmed,Value,Count\n")
		new_csv.close()
	##
	# SAV
	try:
		write_SAV(arguments["sav"], join(arguments["output"], "QCResults", "Report", "src"))
	except:
		print("Couldnt write SAV.")
	##

	if not os.path.exists(join(arguments["output"], "QCResults", "Report", "lib" )):
		shutil.copytree(join(os.path.dirname(__file__),"lib"),join(arguments["output"], "QCResults", "Report", "lib" ))
	if arguments["technology"].startswith("Illumina"):
		technology = "Illumina"
	else:
		technology = "IonTorrent"

	if arguments["forAssembly"]:
		if not arguments["trimBetter_threshold"]:
			arguments["trimBetter_threshold"] = parameter["forAssembly." + technology]["trimBetter_threshold"]
		if not arguments["trimOption"]:
			arguments["trimOption"] = parameter["forAssembly." + technology]["trimOption"]

	elif arguments["forMapping"]:
		if not arguments["trimBetter_threshold"]:
			arguments["trimBetter_threshold"] = parameter["forMapping." + technology]["trimBetter_threshold"]
		if not arguments["trimOption"]:
			arguments["trimOption"] = parameter["forMapping." + technology]["trimOption"]

	if not arguments["trimOption"]:
		arguments["trimOption"] = parameter["Trimmomatic"]["trimOption"]
	if not arguments["trimBetter_threshold"]:
		arguments["trimBetter_threshold"] = parameter["Trimmomatic"]["trimBetter_threshold"]

	# check input
	pattern = re.match("(?P<option>\w+):(?P<value1>\d+):(?P<value2>\d+)", arguments["trimOption"])
	if(arguments["trimOption"]!=""):
		if not ((pattern.group("option") =="SLIDINGWINDOW") or (pattern.group("option") =="MAXINFO")):
			sys.exit("Check trimOption. Only 'SLIDINGWINDOW' or 'MAXINFO' allowed")
		try:
			int(pattern.group("value1"))
			int(pattern.group("value2"))
		except:
			sys.exit("Check trimOptions.")

	if arguments['technology'] == "Illumina":
		if not arguments['adapter']:
			sys.exit("Illumina projects requires an adapter file")
	if arguments["nomapping"]:
		#print("no mapping")
		arguments["reference"] = ""
	else:
		if not arguments["reference"]:
			sys.exit("Mapping needs a reference.")
		else:
			if not os.path.exists(arguments["reference"]):
				sys.exit("Reference does not exist.")

	if not arguments["nokraken"]:
		if not os.path.exists(arguments["db"]):
			sys.exit(arguments["db"] + " does not exist. Enter a valid database for kraken")
		else:
			if "database.kdb" not in os.listdir(arguments["db"]):
				sys.exit("database "+arguments["db"] +" does not contain necessary file database.kdb")
	if not arguments["input"]:
		if arguments["r1"]:
			print(os.path.abspath(arguments["r1"]),os.path.exists(arguments["r1"]))
			if not os.path.exists(arguments["r1"]):
				sys.exit(arguments["r1"] + " does not exist.")
			#if arguments["technology"]=="IonTorrent":
			#	arguments["input"] = arguments["r1"]
			#	arguments["r1"] = ""
		else:
			sys.exit("Input file required. Use option -input or -1 / -2")

		if arguments["r2"]:
			if not os.path.exists(arguments["r2"]):
				sys.exit(arguments["r2"] + " does not exist.")

	if arguments["index"]:
		mapping_index_path = arguments["index"]
	else:
		mapping_index_path = join(tmp, "bowtie2_index")
		os.makedirs(mapping_index_path, exist_ok=True)
	# RUN
	try:
		jsonfile_dict = []
		kraken_reports = OrderedDict()
		#boxplot_data = []
		i = 1
		project = []
		overview = []
		myFiles = []

		#filter samples and put it into for loop
		# Get folder and filter by last three pattern splitted by "_"
		if arguments["input"]:
			input = [ join(arguments["input"], x) for x in sorted(os.listdir(arguments["input"]))]
			#### check input format ####
			# Case 1: Folder contains fastq files
			if len([x for x in input if isfile(x) and any([x.endswith(ext) for ext in file_extensions]) ])>0:
				input= [x for x in input if isfile(x) and any([x.endswith(ext) for ext in file_extensions]) ]
				if arguments["technology"].startswith("Illumina"):
					for setname, files in groupby(input, key=lambda x: "_".join(x.split("_")[:-3])):
						#for setname, files in groupby(files, key=lambda x: "_".join(x.split("_")[:-2])):
						temp_file =  sorted(list(files))
						r1_reads = [x for x in  temp_file if r1_pattern in x]
						r2_reads = [x for x in temp_file if r2_pattern in x]
						myFiles.append([r1_reads, r2_reads])
				else:
					# IonTorrent
					#for setname, files in groupby(input, key=lambda x: "_".join(x.split("_")[:-2])):
					#	temp_file = sorted(list(files))
					# TODO Changed
					myFiles = input
					#r1_reads = [x for x in input if r1_pattern in x]
					#r2_reads = [x for x in input if r2_pattern in x]
					#myFiles.append([r1_reads, r2_reads])

			# Case 2: Folder contains subfolder for each sample
			else:
				input = [x for x in input if os.path.isdir(x)]

				if len(input) > 0:
					for sample in input:
						files = sorted([join(sample,x) for x in listdir(sample) if any([x.endswith(ext) for ext in file_extensions])])
						if arguments["technology"].startswith("Illumina"):# and len(files) > 1:
							for setname, files in groupby(files, key=lambda x: "_".join(x.split("_")[:-3])):
								temp_file = sorted(list(files))
								r1_reads = [x for x in temp_file if r1_pattern in x]
								r2_reads = [x for x in temp_file if r2_pattern in x]
								myFiles.append([r1_reads, r2_reads])
						else:
							myFiles.append(sorted(list(files)))
					files = None
			if len(myFiles) == 0:
					sys.exit("Enter a valid folder, which contain sample folders")
		else:
			if arguments["r1"]:
				if not os.path.exists(arguments["r1"]):
					sys.exit(arguments["r1"] + " does not exist.")

			if arguments["r2"]:
				myFiles.append([arguments["r1"], arguments["r2"]])
			else:
				myFiles.append([arguments["r1"]])

		print("Found " + str(len(myFiles)) + " sample(s).")
		for subset in myFiles:
			print("Analyse " + str(i) + "/" + str(len(myFiles)))

			sample_tmp = join(tmp, "Sample_" + str(i))
			os.makedirs(sample_tmp, exist_ok=True)

			if isinstance(subset, str):
				arguments["r1"] = subset
			else:
				arguments["r1"] = subset[0]
				if len(subset)>1:
					arguments["r2"] = subset[1]

			sample = fillClasses( mapping_index_path, sample_tmp )
			if sample is not None:
				project.append(sample)
				try:
					writeReport(sample, arguments)
				except:
					print("Couldnt write pdf.")
				try:
					if sample.mappingRes is None:
						pMapping = 0
						nMapping = 0
					else:
						pMapping = sample.mappingRes.percentAlignedReads
						nMapping = sample.mappingRes.numberOfAlignedReads
					if sample.krakenRes is None:
						kraken = 0
						kraken_reports ={}
					else:
						kraken = sample.krakenRes.pClassified
						kraken_reports[sample.name] = json.loads(str_to_json(sample.krakenRes.report))
					jsonfile_dict.append(
							OrderedDict([("setname", sample.name),
										 ("Map to reference [#]", nMapping),
										 ("Map to reference [%]", pMapping),
										 ("Reads after trimming [#]", sample.nTrimmed()),
										 ("Reads after trimming [%]", sample.pTrimmed()),
										 ("Classified (Kraken)", kraken),
										 ("Shorter fragments", sample.get_mean_trimRes()),
										 ("Trim Parameter", sample.readSets[0].trimRes.print_param(arguments["palindrome"],arguments["minlen"],arguments["trimOption"])),
										 ("images", [x.get_all_qc_dict() for x in sample.readSets])]))
					#boxplot_data.append(sample.get_all_qc_plots())
				except:
					print("Couldnt produce HTML report.")
			sample = None
			i+=1

		try:
			#env = Environment(block_start_string='@@', block_end_string='@@', variable_start_string='@=', variable_end_string='=@')
			#env.loader = FileSystemLoader(os.path.dirname(__file__))
			#template = env.get_template("batch_report.html")

			#batch_report = template.render(json_samples=json.dumps(jsonfile_dict),
			#							   commandline=json.dumps(arguments), kraken = json.dumps(kraken_reports))
			json.dump({"summary": jsonfile_dict, "commandline":arguments, "kraken":kraken_reports},open(join(arguments["output"], "QCResults/Report/src", "summary.json"),"w"))

			# plot boxplots
			boxplots = [{"file":"Per_sequence_quality_scores.csv",
						 "output": join(arguments["output"], "QCResults/Report/src/img", "Per_sequence_quality_scores.png"),
						 "title": "Per sequence quality scores",
						 "ylab": "Mean Sequence Quality (Phred Score)",
						"xlab": "Sample" },
						{"file": "Sequence_Length_Distribution.csv",
						 "output": join(arguments["output"], "QCResults/Report/src/img", "Sequence_Length_Distribution.png"),
						 "title": "Sequence Length Distributiont",
						 "ylab": "Sequence Length (bp)",
						 "xlab": "Sample"},
						{"file": "Per_sequence_GC_content.csv",
						 "output": join(arguments["output"], "QCResults/Report/src/img", "Per_sequence_GC_content.png"),
						 "title": "Per sequence GC content",
						 "ylab": "Mean GC content (%)",
						 "xlab": "Sample"} ]
			for plot in boxplots:
				process = subprocess.Popen(" ".join(["Rscript --vanilla ",join(os.path.dirname(__file__) ,"boxplot.R"),
						join(arguments["output"],"QCResults", "Report", "src", plot["file"]), plot["output"], '"'+ plot["title"]+'"', '"'+plot["xlab"]+'"','"'+ plot["ylab"]+'"']),
						stderr=subprocess.PIPE, stdout= subprocess.PIPE, shell =True)
				for line in iter(process.stderr.readline, b''):
					print(line)
				process.communicate()

			shutil.copy(join(os.path.dirname(__file__), "batch_report.html"), join(arguments["output"], "QCResults", "Report", "batch_report.html"))


			summary_latex_name = join(arguments["output"], "QCResults", "Report", "summary.latex")
			with open(summary_latex_name, "w") as summary_latex:
				summary_latex.write("\\rowcolors{2}{gray!10}{}\n\\begin{table}[H]\\begin{tabular}{lcc}\n")
				summary_latex.write(
					"\\rowcolor{tableBlue} \\textbf{Sample} & \\textbf{Map to reference [\#]} & \\textbf{Map to reference[\%]} & \\textbf{Reads after trimming [\#]} & \\textbf{Reads after trimming [\%]} & \\textbf{Classified (Kraken)} & \\textbf{Shorter fragments [\%]} \\ \n")

				for i in range(0, len(jsonfile_dict)):
					summary_latex.write(
						str(jsonfile_dict[i]["setname"]) + " & " +
						str(jsonfile_dict[i]["Map to reference [#]"]) + " & " +
						str(jsonfile_dict[i]["Map to reference [%]"]) + " & " +
						str(jsonfile_dict[i]["Reads after trimming [#]"]) + " & " +
						str(jsonfile_dict[i]["Reads after trimming [%]"]) + " & " +
						str(jsonfile_dict[i]["Classified (Kraken)"]) + " & " +
						str(jsonfile_dict[i]["Shorter fragments"]) + "\\\\\n"
					)
				summary_latex.write("\\end{tabular}\n")
				summary_latex.write("\\caption{Statistics on reads after trimming. }")
				summary_latex.close()
		except:

			print("Couldnt produce html file.")
	finally:
		shutil.rmtree(tmp)
		#delete_files(join(arguments["output"],"QCResults"), "FastQC", "_fastqc")
		#delete_files(join(arguments["output"],"QCResults"), "Trimmed/FastQC", "_fastqc")
		delete_files(join(arguments["output"],"QCResults"), "FastQC", "_fastqc.html")
		delete_files(join(arguments["output"],"QCResults"), "Trimmed/FastQC", "_fastqc.html")

if __name__ == "__main__":
	config = configparser.ConfigParser()
	config.read(join(dirname(__file__), "config.txt"))
	kraken_db = ""
	if "DEFAULT" in config:
		if "kraken_db" in config["DEFAULT"].keys():
			kraken_db = config["DEFAULT"]["kraken_db"]

	parser = argparse.ArgumentParser()
	parser.add_argument( '-input', dest='input', help = "input sample folder. Illumina filenames have to end with _<lane>_<R1|R2>_number, e.g. Sample_12_345_R1_001.fastq", required=False)
	parser.add_argument('-1' , dest='r1', help = "input file. Illumina filename must not match <project>_<lane>_<R1|R2>_<number> name pattern", required=False)
	parser.add_argument( '-2', dest='r2', help = "input file", required=False)

	parser.add_argument('-output', dest='output', default="")
	parser.add_argument('-technology', dest='technology',choices=['Illumina',"IonTorrent", "Illumina-MiSeq"] ,required = True)
	parser.add_argument('-threads', dest='threads', default = 4, type = int)
	parser.add_argument('-adapter', dest = 'adapter', choices = ['TruSeq2-PE', 'TruSeq2-SE' , 'TruSeq3-PE' , 'TruSeq3-SE' , 'TruSeq3-PE-2', 'NexteraPE-PE' ])
	parser.add_argument('-reference', dest='reference', required = False, help = "Map reads against reference")
	parser.add_argument('-index', dest='index', required = False, help = "Mapping index")
	parser.add_argument('-sav', dest='sav', required=False,help="Illumina folder for SAV. Requires RunInfo.xml, RunParamter.xml and Interop folder.")

	parser.add_argument('-save_mapping', action = "store_true")
	parser.add_argument('-db', dest='db', help='Kraken database', required = False, default= kraken_db) #"/home/andruscha/programs/kraken/minikraken_20141208/"
	parser.add_argument('-palindrome', dest='palindrome', help= 'Use palindrome parameter 30 or 1000 for further analysis. Default:30', default=30, type = int)
	parser.add_argument('-minlen', dest='minlen',help='Minlen parameter for Trimmomatic. Default:50', default=50, type=int)
	parser.add_argument('-trimOption', dest="trimOption", help='Use maxinfo or slidinginfo for Trimmomatic.MAXINFO:<target length>:<strictness> | SLIDINGWINDOW:<window size>:<required quality>. default: SLIDINGWINDOW:4:15', type= str)
	parser.add_argument('-version', action='version', version='%(prog)s v' + __version__)
	parser.add_argument('-nokraken', action = "store_true")
	parser.add_argument('-nomapping', action = "store_true")
	parser.add_argument('-trimBetter', action = "store_true")
	parser.add_argument('-trimBetter_threshold', dest='trimBetter_threshold', help = "Default setting for Illumina: 0.15 and for IonTorrent: 0.25.", required=False, type=float)
	parser.add_argument('-blobplot', action = "store_true")
	parser.add_argument('-forAssembly', action = "store_true", help = "Trim parameter are optimized for assemblies (trim more aggressive).")
	parser.add_argument('-forMapping', action = "store_true", help = "Trim parameter are optimized for mapping (allow more errors).")

	arguments = vars(parser.parse_args())
	main(arguments)
